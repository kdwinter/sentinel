# Sentinel

Sentinel is custom web application monitor, in Go. It checks a preconfigured
list of endpoints in a given interval, every 60 seconds by default. These
endpoints should return a simple json hash containing:

    {"my_service": true, "my_other_service": true}

Any services returning a `false`-status are considered failing. Endpoints not
returning a valid response at all are also considered failing.

Upon detecting any failure, an email notification will be sent to the
configured recipient.

## Configuration

See `applications.yml.example` and `conf.ini.example` for available options.

## Building

    go build -o sentinel main.go

## Running

    ./sentinel

And visit `localhost:3666` in your browser.
