// Sentinel is an application status monitor.
// Copyright (C) 2018 Kenneth De Winter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/smtp"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/kataras/iris"
	"github.com/kataras/iris/sessions"
	"github.com/kataras/iris/websocket"

	"gopkg.in/ini.v1"
	"gopkg.in/yaml.v2"
)

//--------------------------------------------------------------------------

const (
	CONFIG_PATH  = "./conf.ini"
	MONITOR_PATH = "./applications.yml"
	TEMPLATE_DIR = "./templates"
	PUBLIC_DIR   = "./public"

	COOKIE_AUTH_KEY = "authenticated"
)

//--------------------------------------------------------------------------

var (
	sentinelConfiguration *ini.File
	authToken             string
	refreshInterval       int

	session *sessions.Session

	wsConnections     = make(map[websocket.Connection]bool)
	wsConnectionMutex = new(sync.Mutex)

	applicationsToMonitor []*MonitoredApplication
)

//--------------------------------------------------------------------------

type ApplicationConfiguration struct {
	MonitoredApplications []*MonitoredApplication `yaml:"applications,flow"`
}

func (c *ApplicationConfiguration) Load() *ApplicationConfiguration {
	yamlFile, err := ioutil.ReadFile(MONITOR_PATH)
	if err != nil {
		fmt.Println("Couldn't read application configuration YAML.")
		os.Exit(1)
	}

	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		fmt.Println("Couldn't marshal application configuration YAML.")
		os.Exit(1)
	}

	return c
}

//--------------------------------------------------------------------------

type MonitoredApplication struct {
	Identifier string `yaml:"identifier" json:"identifier"`
	HumanName  string `yaml:"human_name" json:"-"`
	URL        string `yaml:"url" json:"-"`

	Status struct {
		Error    bool            `json:"error"`
		Messages map[string]bool `json:"messages"`
	} `json:"status"`

	Notified bool `json:"-"`
}

func (a *MonitoredApplication) UpdateStatus() {
	delete(a.Status.Messages, "unreachable")
	delete(a.Status.Messages, "unparseable")

	httpClient := http.Client{Timeout: time.Second * 20}
	request, _ := http.NewRequest(http.MethodGet, a.URL, nil)

	response, err := httpClient.Do(request)
	if err != nil {
		a.Status.Error = true
		a.Status.Messages["unreachable"] = true
		return
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		a.Status.Error = true
		a.Status.Messages["unparseable"] = true
		return
	}

	json.Unmarshal(body, &a)

	if a.Status.Error {
		fmt.Println("Encountered an error in " + a.Identifier + ": " +
			fmt.Sprintf("%v", a.Status.Messages))
	}
}

func (a *MonitoredApplication) StatusJson() string {
	j, _ := json.Marshal(a)

	// Add identifier
	//var m map[string]interface{}
	//_ = json.Unmarshal(j, &m)
	//m["identifier"] = a.Identifier
	//j, _ = json.Marshal(m)

	return string(j)
}

func (a *MonitoredApplication) EmailErrorDetails() {
	recipient := sentinelConfiguration.Section("").Key("notification_recipient").String()
	fmt.Println("Emailing " + recipient + " for " + a.Identifier)

	smtpSettings := sentinelConfiguration.Section("smtp")
	auth := smtp.PlainAuth(
		"",
		smtpSettings.Key("username").String(),
		smtpSettings.Key("password").String(),
		smtpSettings.Key("server").String(),
	)
	body := []byte("To: " + recipient + "\r\n" +
		"From: " + smtpSettings.Key("from").String() + "\r\n" +
		"Subject: Sentinel monitored an error in " + a.Identifier + "\r\n" +
		"\r\n" +
		"An error exists in " + a.Identifier + ":\r\n" +
		fmt.Sprintf("%v", a.Status.Messages) + "\r\n")

	smtp.SendMail(
		smtpSettings.Key("server").String()+":"+smtpSettings.Key("port").String(),
		auth,
		smtpSettings.Key("from").String(),
		[]string{recipient},
		body,
	)
}

//--------------------------------------------------------------------------

func main() {
	var err error

	sentinelConfiguration, err = ini.Load(CONFIG_PATH)
	if err != nil {
		fmt.Println("Couldn't load configuration ini. Aborting.")
		os.Exit(1)
	}

	refreshInterval = sentinelConfiguration.Section("").Key("refresh_interval").MustInt(60)
	authToken = sentinelConfiguration.Section("auth").Key("token").String()
	if len(authToken) == 0 {
		fmt.Println("No auth token set in configuration. Aborting.")
		os.Exit(1)
	}

	// Cookie session properties
	//------------------------------------------------------------------
	session := sessions.New(sessions.Config{
		Cookie:       sentinelConfiguration.Section("cookies").Key("name").String(),
		AllowReclaim: true,
		Expires:      time.Hour * 24,
	})

	web := iris.Default()

	// Static assets (scripts, stylesheets, images)
	//------------------------------------------------------------------
	web.StaticWeb("/", PUBLIC_DIR)

	// Dynamic view templates
	//------------------------------------------------------------------
	web.RegisterView(iris.HTML(TEMPLATE_DIR, ".html").Layout("layout.html").Reload(true))

	// Websocket handler
	//------------------------------------------------------------------
	ws := websocket.New(websocket.Config{})
	web.Get("/websocket", ws.Handler())
	web.Any("/js/iris-ws.js", func(ctx iris.Context) {
		ctx.Write(ws.ClientSource)
	})
	ws.OnConnection(func(conn websocket.Connection) {
		ctx := conn.Context()
		ip := ctx.RemoteAddr()
		sess := session.Start(ctx)

		ctx.Application().Logger().Infof("WS: %s connected", ip)

		if !IsAuthenticated(sess) {
			ctx.Application().Logger().Infof("WS: %s is not authenticated; bailing early", ip)

			conn.Disconnect()
			return
		}

		wsConnectionMutex.Lock()
		wsConnections[conn] = true
		// Emit status on initial connection
		for _, application := range applicationsToMonitor {
			// Sends to all _except_ this socket
			//conn.To(websocket.Broadcast).Emit("status", status.AsJSon())
			conn.Emit("status", application.StatusJson())
		}
		wsConnectionMutex.Unlock()

		conn.On("getstatus", func(msg string) {
			ctx.Application().Logger().Infof("WS: %s requested status for %s; emitting `status`", ip, msg)

			wsConnectionMutex.Lock()
			for _, application := range applicationsToMonitor {
				conn.Emit("status", application.StatusJson())
			}
			wsConnectionMutex.Unlock()
		})

		conn.OnDisconnect(func() {
			wsConnectionMutex.Lock()
			delete(wsConnections, conn)
			wsConnectionMutex.Unlock()

			ctx.Application().Logger().Infof("WS: %s disconnected", ip)
		})
	})

	// GET /
	//------------------------------------------------------------------
	web.Get("/", func(ctx iris.Context) {
		sess := session.Start(ctx)

		ctx.ViewData("title", "Monitor")
		ctx.ViewData("applications", applicationsToMonitor)
		RenderView(ctx, sess, "index.html")
	})

	// GET /auth
	//------------------------------------------------------------------
	web.Get("/auth", func(ctx iris.Context) {
		sess := session.Start(ctx)

		if IsAuthenticated(sess) {
			sess.SetFlash("alert", "Already logged in.")
			ctx.Redirect("/")

			return
		}

		ctx.ViewData("title", "Authenticate")
		RenderView(ctx, sess, "auth.html")

	})

	// POST /auth
	//------------------------------------------------------------------
	web.Post("/auth", func(ctx iris.Context) {
		sess := session.Start(ctx)

		if ctx.FormValue("token") == authToken {
			sess.Set(COOKIE_AUTH_KEY, true)
			sess.SetFlash("notice", "Authentication successful.")
			ctx.Redirect("/")
		} else {
			sess.SetFlash("alert", "Invalid credentials.")
			ctx.Redirect("/auth")
		}
	})

	// ANY /logout
	//------------------------------------------------------------------
	web.Any("/logout", func(ctx iris.Context) {
		sess := session.Start(ctx)

		if IsAuthenticated(sess) {
			sess.Set(COOKIE_AUTH_KEY, false)
			sess.SetFlash("notice", "Logout successful.")
			ctx.Redirect("/")
		} else {
			sess.SetFlash("alert", "You're not logged in.")
			ctx.Redirect("/")
		}

	})

	//web.Get("/status", func(ctx iris.Context) {
	//	sess := session.Start(ctx)
	//	if !IsAuthenticated(sess) {
	//		ctx.StatusCode(iris.StatusForbidden)
	//		return
	//	}
	//	ctx.JSON(iris.Map{"status": "ok"})
	//})

	// Error handling
	//------------------------------------------------------------------
	web.OnAnyErrorCode(func(ctx iris.Context) {
		err := iris.Map{
			"status":  ctx.GetStatusCode(),
			"message": ctx.Values().GetString("message"),
		}

		if jsonOutput := ctx.URLParamExists("json"); jsonOutput {
			ctx.JSON(err)
			return
		}

		ctx.ViewData("error", err)
		ctx.ViewData("title", "Error")
		RenderView(ctx, session.Start(ctx), "error.html")
	})

	// Start status goroutines
	//------------------------------------------------------------------
	applicationsToMonitor = new(ApplicationConfiguration).Load().MonitoredApplications
	for _, application := range applicationsToMonitor {
		// Make sure the checks don't start with an empty message map
		application.Status.Messages = make(map[string]bool)
	}
	go CheckAndBroadcastApplicationHealth()

	// Run server
	//------------------------------------------------------------------
	web.Configure(iris.WithCharset("UTF-8"))
	port := sentinelConfiguration.Section("server").Key("http_port").MustString("3000")
	web.Run(iris.Addr(":" + port))
}

//--------------------------------------------------------------------------

func RenderView(ctx iris.Context, sess *sessions.Session, view string) {
	ctx.ViewData("app_name", sentinelConfiguration.Section("").Key("app_name").MustString("Sentinel"))
	ctx.ViewData("authenticated", IsAuthenticated(sess))

	if notice := sess.GetFlashString("notice"); len(notice) > 0 {
		ctx.ViewData("notice", notice)
	}
	if alert := sess.GetFlashString("alert"); len(alert) > 0 {
		ctx.ViewData("alert", alert)
	}

	ctx.View(view)
}

func IsAuthenticated(sess *sessions.Session) bool {
	authenticated, err := sess.GetBoolean(COOKIE_AUTH_KEY)
	if err != nil {
		return false
	}
	return authenticated
}

func CheckAndBroadcastApplicationHealth() {
	fmt.Printf("Starting healthcheck goroutine, running every %d seconds\n", refreshInterval)

	shouldNotify := sentinelConfiguration.Section("").Key("enable_notifications").MustBool(false)

	for {
		wsConnectionMutex.Lock()
		for conn := range wsConnections {
			conn.Emit("next_status_in", strconv.Itoa(refreshInterval))
		}
		wsConnectionMutex.Unlock()

		var wg sync.WaitGroup

		for _, application := range applicationsToMonitor {
			wg.Add(1)

			go func(application *MonitoredApplication) {
				defer wg.Done()

				application.UpdateStatus()

				wsConnectionMutex.Lock()
				for conn := range wsConnections {
					conn.Emit("status", application.StatusJson())
				}
				wsConnectionMutex.Unlock()

				if shouldNotify {
					if application.Status.Error {
						if !application.Notified {
							application.EmailErrorDetails()
							application.Notified = true

						}
					} else if application.Notified {
						application.Notified = false
					}
				}
			}(application)
		}

		wg.Wait()

		time.Sleep(time.Second * time.Duration(refreshInterval))
	}
}
