Sentinel.secondsUntilUpdate = 0;

Sentinel.registerBootstrapEvents = (() => {
  $('*[data-toggle="popover"]').popover();
  $('*[data-toggle="tooltip"]').tooltip();
});

Sentinel.registerWebsocket = (() => {
  const scheme  = document.location.protocol === "https" ? "wss" : "ws";
  const port    = document.location.port ? (":"+document.location.port) : "";
  const wsUrl   = scheme + "://" + document.location.hostname + port + "/websocket";
  const socket  = new Ws(wsUrl);

  let connected = false;

  const updateTimeleft = (() => {
    if(connected) {
      let seconds = Sentinel.secondsUntilUpdate;
      if(seconds >= 0) {
        seconds = --Sentinel.secondsUntilUpdate;
      }

      $(".timeleft").html(`Next autorefresh in ${seconds}s`);
    }
  });

  const setWsStatus = ((status) => {
    const el = $("#ws_status");

    switch(status) {
    case "connected":
      el.removeClass("bg-danger").addClass("bg-success");
      el.html("Connected");
      break;
    case "disconnected":
      el.removeClass("bg-success").addClass("bg-danger");
      el.html("Disconnected");
      break;
    }
  });

  $("a.update-status").on("click", (e) => {
    $(".status").removeClass("bg-danger").removeClass("bg-success").addClass("bg-dark").html("PENDING");
    socket.Emit("getstatus", "all");
  });

  socket.OnConnect(() => {
    console.info("WS: Connected");

    connected = true;
    setWsStatus("connected");
  });

  socket.OnDisconnect(() => {
    console.error("WS: Disconnected. Reload the page or find out what's wrong");

    connected = false;
    setWsStatus("disconnected");
    if(Sentinel.timeleftUpdateInterval) clearInterval(Sentinel.timeleftUpdateInterval);
  });

  socket.On("status", (msg) => {
    console.info("WS: Status received: " + msg);

    const application = JSON.parse(msg);
    const status = application.status;
    const el = $(`.list-group-item[data-identifier="${application.identifier}"]`);

    if(status.error) {
      el.find(".status").removeClass("bg-success").removeClass("bg-dark").addClass("bg-danger").html("ERRORS");
    } else {
      el.find(".status").removeClass("bg-danger").removeClass("bg-dark").addClass("bg-success").html("OK");
    }
    el.find(".status-messages").html(`<pre class="hl-json">${syntaxHighlightJson(JSON.stringify(status.messages, undefined, 4))}</pre>`);
  });

  socket.On("next_status_in", (msg) => {
    Sentinel.secondsUntilUpdate = parseInt(msg);

    if(Sentinel.timeleftUpdateInterval) clearInterval(Sentinel.timeleftUpdateInterval);
    Sentinel.timeleftUpdateInterval = setInterval(updateTimeleft, 1000);
  });
});

const SHRX = /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g;
const syntaxHighlightJson = ((json) => {
  json = json.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");

  return json.replace(SHRX, (match) => {
    let cls = "number";

    if(/^"/.test(match)) {
      if(/:$/.test(match)) {
        cls = "key";
      } else {
        cls = "string";
      }
    } else if(/true|false/.test(match)) {
      cls = "boolean";
    } else if(/null/.test(match)) {
      cls = "null";
    }

    return `<span class="${cls}">${match}</span>`;
  });
});

document.addEventListener("DOMContentLoaded", () => {
  Sentinel.registerBootstrapEvents();
  if(Sentinel.authenticated) Sentinel.registerWebsocket();
});
